package com.yslivka.thebeegame.entities;

import android.support.annotation.VisibleForTesting;

public class Bee {

    private final static int QUEEN_MAX_HP = 100;
    @VisibleForTesting
    public final static int QUEEN_DAMAGE_PER_HIT = 8;
    private final static int WORKER_MAX_HP = 75;
    @VisibleForTesting
    public final static int WORKER_DAMAGE_PER_HIT = 10;
    private final static int DRONE_MAX_HP = 50;
    @VisibleForTesting
    public final static int DRONE_DAMAGE_PER_HIT = 12;

    public final static int QUEEN = 0;
    public final static int WORKER = 1;
    public final static int DRONE = 2;

    private final int damagePerHit;
    private int health;
    private int role;

    public Bee(int role) {
        this.role = role;
        switch (role) {
            case QUEEN:
                health = QUEEN_MAX_HP;
                damagePerHit = QUEEN_DAMAGE_PER_HIT;
                break;
            case WORKER:
                health = WORKER_MAX_HP;
                damagePerHit = WORKER_DAMAGE_PER_HIT;
                break;
            case DRONE:
                health = DRONE_MAX_HP;
                damagePerHit = DRONE_DAMAGE_PER_HIT;
                break;
            default:
                throw new IllegalArgumentException("Wrong role");
        }
    }

    public int getCurrentHealth() {
        return health;
    }

    public int getMaxHealth() {
        switch (role) {
            case QUEEN:
                return QUEEN_MAX_HP;
            case WORKER:
                return WORKER_MAX_HP;
            case DRONE:
                return DRONE_MAX_HP;
            default:
                throw new IllegalArgumentException("Wrong role");
        }
    }

    public boolean hit() {
        health -= damagePerHit;
        return health > 0;
    }

    public int getRole() {
        return role;
    }
}
