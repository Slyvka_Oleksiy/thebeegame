package com.yslivka.thebeegame;

import android.os.Bundle;

import com.squareup.otto.Subscribe;
import com.yslivka.thebeegame.events.ReturnToMenuEvent;
import com.yslivka.thebeegame.events.StartNewGameEvent;
import com.yslivka.thebeegame.game.GameFragment;
import com.yslivka.thebeegame.library.PresenterActivity;
import com.yslivka.thebeegame.menu.MenuFragment;

import static com.yslivka.thebeegame.App.getBus;

public class MainActivity extends PresenterActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new MenuFragment())
                    .commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getBus(this).register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getBus(this).unregister(this);
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onStartNewGameEvent(StartNewGameEvent event) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new GameFragment())
                .commit();
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onReturnToMenuEvent(ReturnToMenuEvent event) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new MenuFragment())
                .commit();
    }
}
