package com.yslivka.thebeegame.menu;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yslivka.thebeegame.R;
import com.yslivka.thebeegame.events.StartNewGameEvent;
import com.yslivka.thebeegame.library.BasePresenterFragment;

import static com.yslivka.thebeegame.App.getBus;

public class MenuFragment extends BasePresenterFragment<MenuView, MenuPresenter> implements MenuView {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        view.findViewById(R.id.newGameButton).setOnClickListener(v -> onNewGameButtonClicked());
        return view;
    }

    @NonNull
    @Override
    public MenuPresenter onCreatePresenter() {
        return new MenuPresenter();
    }

    @Override
    public void startNewGame() {
        getBus(getActivity()).post(new StartNewGameEvent());
    }

    public void onNewGameButtonClicked() {
        getPresenter().onNewGameButtonClicked();
    }
}
