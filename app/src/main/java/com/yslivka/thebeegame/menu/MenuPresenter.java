package com.yslivka.thebeegame.menu;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.yslivka.thebeegame.library.BasePresenter;
import com.yslivka.thebeegame.library.PresenterBundle;

class MenuPresenter extends BasePresenter<MenuView> {

    private static final String SERIAL_KEY = "serial";

    private int serial = -1;

    @Override
    public void bindView(MenuView view) {
        super.bindView(view);
    }

    @Override
    public void onCreate(@Nullable PresenterBundle bundle) {
        if (bundle != null) {
            serial = bundle.getInt(SERIAL_KEY);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull PresenterBundle bundle) {
        bundle.putInt(SERIAL_KEY, serial);
    }

    void onNewGameButtonClicked() {
        view.startNewGame();
    }
}
