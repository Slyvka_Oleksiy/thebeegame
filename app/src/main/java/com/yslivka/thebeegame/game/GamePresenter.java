package com.yslivka.thebeegame.game;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import com.yslivka.thebeegame.entities.Bee;
import com.yslivka.thebeegame.library.BasePresenter;
import com.yslivka.thebeegame.library.PresenterBundle;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class GamePresenter extends BasePresenter<GameView> {

    private static final int QUEEN_COUNT = 1;
    private static final int WORKER_COUNT = 5;
    private static final int DRONE_COUNT = 8;

    private static final String SERIAL_KEY = "serial";

    private int serial = -1;

    private List<Bee> bees = new ArrayList<>();
    private long time;

    GamePresenter() {
        generateBees();
    }

    @Override
    public void bindView(GameView view) {
        super.bindView(view);
    }

    @Override
    public void onCreate(@Nullable PresenterBundle bundle) {
        if (bundle != null) {
            serial = bundle.getInt(SERIAL_KEY);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull PresenterBundle bundle) {
        bundle.putInt(SERIAL_KEY, serial);
    }

    private void generateBees() {
        for (int i = 0; i < QUEEN_COUNT; i++) {
            bees.add(new Bee(Bee.QUEEN));
        }
        for (int i = 0; i < WORKER_COUNT; i++) {
            bees.add(new Bee(Bee.WORKER));
        }
        for (int i = 0; i < DRONE_COUNT; i++) {
            bees.add(new Bee(Bee.DRONE));
        }
    }

    void onSurfaceReady() {
        view.showBees(bees);
    }

    void onFinalDialogCanceled() {
        view.returnToMenu();
    }

    void onHitButtonClicked() {
        if (bees.size() > 0) {
            int index = getRandomBeeIndex();
            hitBee(index);
        }
    }

    @VisibleForTesting
    void hitBee(int index) {
        Bee bee = bees.get(index);
        boolean isAlive = bee.hit();
        if (!isAlive) {
            if (bee.getRole() == Bee.QUEEN) {
                bees.clear();
                time = System.currentTimeMillis() - time;
                view.killAllBees();
                view.showFinalDialog(time);
            } else {
                bees.remove(bee);
                view.killBee(index);
            }
        } else {
            view.updateBee(index, bee.getCurrentHealth());
        }
    }

    private int getRandomBeeIndex() {
        return new Random().nextInt(bees.size());
    }

    void startTimer() {
        time = System.currentTimeMillis() - time;
    }

    void stopTimer() {
        time = System.currentTimeMillis() - time;
    }

    void onStartNewGameClicked() {
        view.startNewGame();
    }

    @VisibleForTesting
    List<Bee> getBees() {
        return bees;
    }

    @VisibleForTesting
    long getTime() {
        return time;
    }
}
