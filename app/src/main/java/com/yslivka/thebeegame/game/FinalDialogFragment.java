package com.yslivka.thebeegame.game;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import java.util.concurrent.TimeUnit;

public class FinalDialogFragment extends DialogFragment {

    private static final String EXTRA_TIME = FinalDialogFragment.class.getName() + "extra:time";
    private OnStartNewGameClickedListener onStartNewGameClickedListener;
    private OnFinalDialogCanceledListener onFinalDialogCanceledListener;

    public interface OnStartNewGameClickedListener {
        void onStartNewGameClicked();
    }

    public interface OnFinalDialogCanceledListener {
        void onFinalDialogCanceled();
    }

    public static FinalDialogFragment create(long time) {
        FinalDialogFragment finalDialogFragment = new FinalDialogFragment();
        Bundle args = new Bundle();
        args.putLong(EXTRA_TIME, time);
        finalDialogFragment.setArguments(args);
        return finalDialogFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        long time = getArguments().getLong(EXTRA_TIME);
        String string = "Congratulation! Your time: "
                + TimeUnit.MILLISECONDS.toMinutes(time) + " min, "
                + (TimeUnit.MILLISECONDS.toSeconds(time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time))) + " sec";
        return new AlertDialog.Builder(getContext())
                .setMessage(string)
                .setPositiveButton("Start new game", (dialogInterface, i) -> onStartNewGameClickedListener.onStartNewGameClicked())
                .create();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        onFinalDialogCanceledListener.onFinalDialogCanceled();
    }

    public void setOnStartNewGameClickedListener(OnStartNewGameClickedListener onStartNewGameClickedListener) {
        this.onStartNewGameClickedListener = onStartNewGameClickedListener;
    }

    public void setOnFinalDialogCanceledListener(OnFinalDialogCanceledListener onFinalDialogCanceledListener) {
        this.onFinalDialogCanceledListener = onFinalDialogCanceledListener;
    }
}