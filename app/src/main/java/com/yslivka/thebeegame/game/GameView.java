package com.yslivka.thebeegame.game;

import com.yslivka.thebeegame.entities.Bee;

import java.util.List;

interface GameView {
    void showBees(List<Bee> bees);

    void startNewGame();

    void returnToMenu();

    void showFinalDialog(long time);

    void updateBee(int index, int health);

    void killBee(int index);

    void killAllBees();
}
