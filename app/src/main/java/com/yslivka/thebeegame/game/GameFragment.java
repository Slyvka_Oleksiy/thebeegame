package com.yslivka.thebeegame.game;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.yslivka.thebeegame.entities.Bee;
import com.yslivka.thebeegame.R;
import com.yslivka.thebeegame.events.ReturnToMenuEvent;
import com.yslivka.thebeegame.events.StartNewGameEvent;
import com.yslivka.thebeegame.library.BasePresenterFragment;

import java.util.List;

import static com.yslivka.thebeegame.App.getBus;

public class GameFragment extends BasePresenterFragment<GameView, GamePresenter>
        implements GameView, SurfaceHolder.Callback, FinalDialogFragment.OnStartNewGameClickedListener, FinalDialogFragment.OnFinalDialogCanceledListener {

    private static final String FINAL_DIALOG_TAG = "FINAL_DIALOG";

    private DrawingThread drawingThread;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_game, container, false);

        SurfaceView surfaceView = (SurfaceView) view.findViewById(R.id.surface);
        surfaceView.getHolder().addCallback(this);

        view.findViewById(R.id.floatingActionButton).setOnClickListener(v -> getPresenter().onHitButtonClicked());

        if (savedInstanceState != null) {
            FinalDialogFragment finalDialogFragment = (FinalDialogFragment) getFragmentManager().findFragmentByTag(FINAL_DIALOG_TAG);
            if (finalDialogFragment != null) {
                finalDialogFragment.setOnStartNewGameClickedListener(this);
                finalDialogFragment.setOnFinalDialogCanceledListener(this);
            }
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getPresenter().startTimer();
    }

    @Override
    public void onPause() {
        super.onPause();
        getPresenter().stopTimer();
    }

    @NonNull
    @Override
    public GamePresenter onCreatePresenter() {
        return new GamePresenter();
    }

    @Override
    public void showBees(List<Bee> bees) {
        drawingThread.addItems(bees);
    }

    @Override
    public void startNewGame() {
        getBus(getActivity()).post(new StartNewGameEvent());
    }

    @Override
    public void returnToMenu() {
        getBus(getActivity()).post(new ReturnToMenuEvent());
    }

    @Override
    public void showFinalDialog(long time) {
        FinalDialogFragment finalDialogFragment = FinalDialogFragment.create(time);
        finalDialogFragment.setOnStartNewGameClickedListener(this);
        finalDialogFragment.setOnFinalDialogCanceledListener(this);
        finalDialogFragment.show(getFragmentManager(), FINAL_DIALOG_TAG);
    }

    @Override
    public void onStartNewGameClicked() {
        getPresenter().onStartNewGameClicked();
    }

    @Override
    public void onFinalDialogCanceled() {
        getPresenter().onFinalDialogCanceled();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        drawingThread = new DrawingThread(holder,
                BitmapFactory.decodeResource(getResources(), R.drawable.ic_bee_queen),
                BitmapFactory.decodeResource(getResources(), R.drawable.ic_bee_worker),
                BitmapFactory.decodeResource(getResources(), R.drawable.ic_bee_drone));
        drawingThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        drawingThread.updateSize(width, height);
        getPresenter().onSurfaceReady();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        drawingThread.quit();
        drawingThread = null;
    }

    @Override
    public void updateBee(int index, int health) {
        drawingThread.updateItem(index, health);
    }

    @Override
    public void killBee(int index) {
        drawingThread.removeItem(index);
    }

    @Override
    public void killAllBees() {
        drawingThread.clearItems();
    }
}
