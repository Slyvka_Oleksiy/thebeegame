package com.yslivka.thebeegame.game;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.v4.util.Pair;
import android.view.SurfaceHolder;

import com.yslivka.thebeegame.entities.Bee;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class DrawingThread extends HandlerThread implements Handler.Callback {

    private static final int MSG_MOVE = 101;
    private static final int MSG_CLEAR = 102;

    private Handler receiver;
    private SurfaceHolder drawingSurface;

    private int drawingWidth, drawingHeight;

    private Paint paint;
    private Paint greenPaint;
    private Paint redPaint;

    private Bitmap queenIcon;
    private Bitmap workerIcon;
    private Bitmap droneIcon;

    private List<DrawingItem> drawingItems;
    private List<Integer> removeItems;
    private List<Pair<Integer, Integer>> updateItems;

    private class DrawingItem {
        int health;
        int maxHealth;
        int x, y;
        boolean horizontal, vertical;
        int role;

        DrawingItem(int health, int maxHealth, int role, int x, int y, boolean horizontal, boolean vertical) {
            this.x = x;
            this.y = y;
            this.health = health;
            this.maxHealth = maxHealth;
            this.horizontal = horizontal;
            this.vertical = vertical;
            this.role = role;
        }
    }

    DrawingThread(SurfaceHolder holder, Bitmap queenIcon, Bitmap workerIcon, Bitmap droneIcon) {
        super("DrawingThread");
        drawingSurface = holder;
        drawingItems = new ArrayList<>();
        removeItems = new ArrayList<>();
        updateItems = new ArrayList<>();
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        greenPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        greenPaint.setStrokeWidth(10);
        greenPaint.setColor(Color.GREEN);
        redPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        redPaint.setStrokeWidth(10);
        redPaint.setColor(Color.RED);
        this.queenIcon = queenIcon;
        this.workerIcon = workerIcon;
        this.droneIcon = droneIcon;
    }

    @Override
    protected void onLooperPrepared() {
        receiver = new Handler(getLooper(), this);
        receiver.sendEmptyMessage(MSG_MOVE);
    }

    @Override
    public boolean quit() {
        receiver.removeCallbacksAndMessages(null);
        return super.quit();
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case MSG_CLEAR:
                drawingItems.clear();
                break;
            case MSG_MOVE:
                Canvas c = drawingSurface.lockCanvas();
                if (c == null) {
                    break;
                }
                c.drawColor(Color.WHITE);
                for (Pair<Integer, Integer> pair : updateItems) {
                    drawingItems.get(pair.first).health = pair.second;
                }
                updateItems.clear();
                for (int index : removeItems) {
                    drawingItems.remove(index);
                }
                removeItems.clear();
                for (DrawingItem item : drawingItems) {
                    Bitmap icon = item.role == Bee.QUEEN ? queenIcon : item.role == Bee.WORKER ? workerIcon : droneIcon;
                    item.x += (item.horizontal ? 5 : -5);
                    if (item.x >= (drawingWidth - icon.getWidth())) {
                        item.horizontal = false;
                    } else if (item.x <= 0) {
                        item.horizontal = true;
                    }
                    item.y += (item.vertical ? 5 : -5);
                    if (item.y >= (drawingHeight - icon.getHeight())) {
                        item.vertical = false;
                    } else if (item.y <= 0) {
                        item.vertical = true;
                    }
                    c.drawBitmap(icon, item.x, item.y, paint);
                    c.drawLine(item.x, item.y, item.x + (icon.getWidth() * item.health / item.maxHealth), item.y, greenPaint);
                    c.drawLine(item.x + (icon.getWidth() * item.health / item.maxHealth), item.y, item.x + icon.getWidth(), item.y, redPaint);
                }
                drawingSurface.unlockCanvasAndPost(c);
                break;
        }
        receiver.sendEmptyMessage(MSG_MOVE);
        return true;
    }

    void updateSize(int width, int height) {
        drawingWidth = width;
        drawingHeight = height;
    }

    void addItems(List<Bee> bees) {
        Random random = new Random();
        int x, y;
        boolean horizontal, vertical;
        for (Bee bee : bees) {
            x = random.nextInt(drawingWidth);
            y = random.nextInt(drawingHeight);
            horizontal = Math.round(Math.random()) == 0;
            vertical = Math.round(Math.random()) == 0;
            DrawingItem newItem = new DrawingItem(bee.getCurrentHealth(), bee.getMaxHealth(), bee.getRole(),
                    x, y, horizontal, vertical);
            drawingItems.add(newItem);
        }
    }

    void clearItems() {
        receiver.sendEmptyMessage(MSG_CLEAR);
    }

    void removeItem(int index) {
        removeItems.add(index);
    }

    void updateItem(int index, int health) {
        updateItems.add(new Pair<>(index, health));
    }
}
