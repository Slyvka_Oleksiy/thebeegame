package com.yslivka.thebeegame.menu;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

public class MenuPresenterTest {

    @Mock
    private MenuView menuView;

    private MenuPresenter menuPresenter;

    @Before
    public void setupMenuPresenter() {
        MockitoAnnotations.initMocks(this);

        menuPresenter = new MenuPresenter();
        menuPresenter.bindView(menuView);
    }

    @Test
    public void onNewGameButtonClicked_checkNewGameStarted() throws Exception {
        menuPresenter.onNewGameButtonClicked();
        verify(menuView).startNewGame();
    }
}