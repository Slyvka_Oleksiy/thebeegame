package com.yslivka.thebeegame.game;

import com.yslivka.thebeegame.entities.Bee;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.mockito.Mockito.verify;

public class GamePresenterTest {
    @Mock
    private GameView gameView;

    private GamePresenter gamePresenter;

    @Before
    public void setupMenuPresenter() {
        MockitoAnnotations.initMocks(this);

        gamePresenter = new GamePresenter();
        gamePresenter.bindView(gameView);
    }

    @Test
    public void onSurfaceReady_checkBeesShown() throws Exception {
        gamePresenter.onSurfaceReady();
        verify(gameView).showBees(gamePresenter.getBees());
    }

    @Test
    public void onFinalDialogCanceled_checkReturnedToMenu() throws Exception {
        gamePresenter.onFinalDialogCanceled();
        verify(gameView).returnToMenu();
    }

    @Test
    public void hitBee_hitQueen_checkDamagePerHit() throws Exception {
        int queenIndex = 0;
        List<Bee> bees = gamePresenter.getBees();
        for (int i = 0; i < bees.size(); i++) {
            if (bees.get(i).getRole() == Bee.QUEEN) {
                queenIndex = i;
                break;
            }
        }
        Bee queen = bees.get(queenIndex);
        int queenHealth = queen.getCurrentHealth();
        gamePresenter.hitBee(queenIndex);
        Assert.assertEquals(queenHealth - queen.getCurrentHealth(), Bee.QUEEN_DAMAGE_PER_HIT);
    }

    @Test
    public void hitBee_hitWorker_checkDamagePerHit() throws Exception {
        int workerIndex = 0;
        List<Bee> bees = gamePresenter.getBees();
        for (int i = 0; i < bees.size(); i++) {
            if (bees.get(i).getRole() == Bee.WORKER) {
                workerIndex = i;
                break;
            }
        }
        Bee worker = bees.get(workerIndex);
        int workerHealth = worker.getCurrentHealth();
        gamePresenter.hitBee(workerIndex);
        Assert.assertEquals(workerHealth - worker.getCurrentHealth(), Bee.WORKER_DAMAGE_PER_HIT);
    }

    @Test
    public void hitBee_hitDrone_checkDamagePerHit() throws Exception {
        int droneIndex = 0;
        List<Bee> bees = gamePresenter.getBees();
        for (int i = 0; i < bees.size(); i++) {
            if (bees.get(i).getRole() == Bee.DRONE) {
                droneIndex = i;
                break;
            }
        }
        Bee drone = bees.get(droneIndex);
        int droneHealth = drone.getCurrentHealth();
        gamePresenter.hitBee(droneIndex);
        Assert.assertEquals(droneHealth - drone.getCurrentHealth(), Bee.DRONE_DAMAGE_PER_HIT);
    }

    @Test
    public void hitBee_killQueen_checkAllBeesDead() throws Exception {
        int queenIndex = 0;
        List<Bee> bees = gamePresenter.getBees();
        for (int i = 0; i < bees.size(); i++) {
            if (bees.get(i).getRole() == Bee.QUEEN) {
                queenIndex = i;
                break;
            }
        }
        Bee queen = bees.get(queenIndex);
        while (queen.getCurrentHealth() > 0) {
            gamePresenter.hitBee(queenIndex);
        }
        verify(gameView).killAllBees();
        verify(gameView).showFinalDialog(gamePresenter.getTime());
    }

    @Test
    public void hitBee_checkBeeKilled() throws Exception {
        int beeIndex = 0;
        List<Bee> bees = gamePresenter.getBees();
        for (int i = 0; i < bees.size(); i++) {
            if (bees.get(i).getRole() != Bee.QUEEN) {
                beeIndex = i;
                break;
            }
        }
        Bee bee = bees.get(beeIndex);
        while (bee.getCurrentHealth() > 0) {
            gamePresenter.hitBee(beeIndex);
        }
        verify(gameView).killBee(beeIndex);
    }

    @Test
    public void onStartNewGameClicked_checkNewGameStarted() throws Exception {
        gamePresenter.onStartNewGameClicked();
        verify(gameView).startNewGame();
    }
}